sshpass  -p $1 ssh -o StrictHostKeyChecking=no admin@infra1.hoggart.eu <<-'ENDSSH'   
   docker stop photoshow
   docker rm photoshow
   docker pull registry.gitlab.com/hogsim/photoshow:master
   docker run --name photoshow --restart=always -p 48325:80  -v /home/simon/WEB/PhotoShow:/var/www/html -v  /home/simon/WEB/data-photoshow:/opt/PhotoShow -d -i -t registry.gitlab.com/hogsim/photoshow:master
ENDSSH
