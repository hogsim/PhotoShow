FROM registry.gitlab.com/hogsim/dockerbase-photoshow:master

RUN git clone https://gitlab.com/hogsim/PhotoShow.git /var/www/PhotoShow
RUN cd /var/www/PhotoShow && sed -i -e 's/$config->photos_dir.\+/$config->photos_dir = "\/opt\/PhotoShow\/Photos";/' config.php
RUN cd /var/www/PhotoShow && sed -i -e 's/$config->ps_generated.\+/$config->ps_generated = "\/opt\/PhotoShow\/generated";/' config.php

VOLUME ["/opt/PhotoShow", "/var/log"]
EXPOSE 80
CMD /usr/bin/supervisord
