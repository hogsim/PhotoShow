<?php
/**
 * This file implements the class Judge.
 * 
 * PHP versions 4 and 5
 *
 * LICENSE:
 * 
 * This file is part of PhotoShow.
 *
 * PhotoShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PhotoShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PhotoShow.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  Website
 * @package   Photoshow
 * @author    Thibaud Rohmer <thibaud.rohmer@gmail.com>
 * @copyright 2011 Thibaud Rohmer
 * @license   http://www.gnu.org/licenses/
 * @link      http://github.com/thibaud-rohmer/PhotoShow
 */

/**
 * Judge
 *
 * The Judge verifies the rights of Current User, and checks
 * if he is allowed to reach some content. No one fools the
 * Judge. After all, the Judge is the Law.
 *
 * @category  Website
 * @package   Photoshow
 * @author    Thibaud Rohmer <thibaud.rohmer@gmail.com>
 * @copyright Thibaud Rohmer
 * @license   http://www.gnu.org/licenses/
 * @link      http://github.com/thibaud-rohmer/PhotoShow
 */

class JudgeUploader
{
	/// Absolute path to rights file for requested file
	public $path;
	
	/// True if requested file is public
	public $public=false;
	
	/// Groups allowed to see requested file
	public $groups=array();
	
	/// Users allowed to see requested file
	public $users=array();
	
	/// Name of requested file
	public $filename;

	/// Urlencoded relative path
	public $webpath;

	/// Path to the file
	public $file;

	/// Are we working with multiple items ?
	private $multi;


	/// Used to display some file infos
	private $infos;

	/**
	 * Create a Judge for a specific file.
	 *
	 * @param string $f 
	 * @param string $read_rights 
	 * @author Thibaud Rohmer
	 */
	public function __construct($f, $read_rights=true){

		if(! is_array($f) && !file_exists($f) ){
			return;
		}
		$this->public	=	false;
		$this->groups	=	array();
		$this->users	=	array();
		$this->file 	=	$f;

		// Multiple files
		if(is_array($f)){
			$this->multi = true;
			$this->filename = sizeof($f) . " files selected";
			$this->webpath = "";
			foreach ($f as $file) {
				$this->webpath .= "&f[]=".urlencode(File::a2r($file));
			}
	        $this->infos 		= "";
		}else{
			$this->multi = false;
			$this->set_path($f);
			if($read_rights){
				$this->set_rights();
			}
	        $this->infos 		= $this->infodirtoHTML($f);

		}

	}
	
	/**
	 * Get path to rights file associated to our file
	 *
	 * @param string $f 
	 * @return void
	 * @author Thibaud Rohmer
	 */
	private function set_path($f){
		
		$basefile	= 	new File($f);
		$basepath	=	File::a2r($f);

		$this->filename = $basefile->name;
		$this->webpath 	= "&f=".urlencode($basepath);

		if(is_file($f)){
			$rightsfile	=	dirname($basepath)."/.".mb_basename($f)."_rights.xml";
		}else{
			$rightsfile	=	$basepath."/.rights.xml";
		}
		$this->path =	File::r2a($rightsfile,Settings::$thumbs_dir);

	}
	
	/**
	 * Get rights (recursively) for the file
	 *
	 * @return void
	 * @author Thibaud Rohmer
	 */
	private function set_rights(){

		/// First, parse the rights file (if it exists)
		try{
			$xml_infos	=	new File($this->path);
			$xml		=	simplexml_load_file($this->path);

			$this->public	=	($xml->public == 1);

			foreach($xml->groups->children() as $g)
				$this->groups[]=(string)$g;

			foreach($xml->users->children() as $u)
				$this->users[]=(string)$u;

		}catch(Exception $e){
		
			/// If no rights file found, check in the containing directory
			try{
				// Look up

				$up		=	dirname($this->file);
				$j = new JudgeUploader($up);
				
				$this->groups 	= $j->groups;
				$this->users 	= $j->users;
				$this->public 	= $j->public;


			}catch(Exception $e){
				
				// We are as high as possible
				$this->public	=	true;
				$this->groups	=	array();
				$this->users	=	array();		
			}
		}
	}

	/**
	 * Check recursively if a file is viewable in a folder, and returns path to that file.
	 */
	public static function searchDir($dir,$public_search = false){

		foreach(Menu::list_files($dir) as $f){
			if(JudgeUploader::view($f)){
				return $f;
			}
		}

		foreach(Menu::list_dirs($dir) as $d){
			if(($f=JudgeUploader::searchDir($d, $public_search)) != NULL){
				return $f;
			}
		}

		return NULL;
	}

	/**
	 * Save our judge for this file as an xml file
	 *
	 * @return void
	 * @author Thibaud Rohmer
	 */
	public function save(){
		
		/// Create xml
		$xml		=	new SimpleXMLElement('<rights></rights>');
		
		/// Put values in xml
		$xml->addChild('public',$this->public);
		$xml_users	=	$xml->addChild('users');
		$xml_groups	=	$xml->addChild('groups');

		foreach($this->users as $user)
			$xml_users->addChild("login",$user);

		foreach($this->groups as $group)
			$xml_groups->addChild("group",$group);
		
		if(!file_exists(dirname($this->path))){
			@mkdir(dirname($this->path),0750,true);
		}
		/// Save xml
		$xml->asXML($this->path);
	}
	
	/**
	 * Edit rights of the Judge. Because you can.
	 *
	 * @param string $f 
	 * @param string $groups 
	 * @param string $users 
	 * @return void
	 * @author Thibaud Rohmer
	 */
	public static function edit($f,$users=array(),$groups=array(),$private=false){

		/// Just to be sure, check that user is admin
		if(!CurrentUser::$admin)
			return;

		if(is_array($f)){
			foreach($f as $file){
				JudgeUploader::edit($file,$users,$groups,$private);
			}
			return;
		}
		// Create new Judge, no need to read its rights
		$rights			=	new JudgeUploader($f,false);

		/// Put the values in the Judge (poor guy)
		if(isset($groups)){
			$rights->groups =	$groups;
		}

		if(isset($users)){
			$rights->users =	$users;
		}
		
		$rights->public	=	( !$private ) ? 1 : 0;
		
		// Save the Judge
		$rights->save();
	}
	
	/**
	 * Returns true if the file to access is in the sub-path of the main directory
	 *
	 * @param string $f file to access
	 * @return bool
	 * @author Thibaud Rohmer
	 */
	public static function inGoodPlace($f){

		$rf =	realpath($f);
		$rd =	realpath(Settings::$photos_dir);
		
		if($rf == $rd) return true;

		if( substr($rf,0,strlen($rd)) == $rd ){
			return true;
		}
		return false;

	}

	/**
	 * Returns true if the current user may access this file
	 *
	 * @param string $f file to access
	 * @return bool
	 * @author Thibaud Rohmer
	 */
	public static function view($f){
		
		// Check if user has an account		
		if(!isset(CurrentUser::$account) && !isset(CurrentUser::$token)){
			// User is not logged in
			$judge	=	new JudgeUploader($f);
			return($judge->public);
		}

		if(!JudgeUploader::inGoodPlace($f))
			return false;

		// No Judge required for the admin. This guy rocks.
		if(CurrentUser::$admin)
			return true;

		// Create Judge
		$judge	=	new JudgeUploader($f);
		
		// Public file
		if($judge->public){
			return true;
		}

        if (isset(CurrentUser::$account)){
            // User allowed
            if(in_array(CurrentUser::$account->login,$judge->users)){
                return true;
            }

            // User in allowed group
            foreach(CurrentUser::$account->groups as $group){
                if(in_array($group,$judge->groups)){
                    return true;
                }
            }
        }
        if (isset(CurrentUser::$token)){
            if (GuestToken::view(CurrentUser::$token,$f)){
                return true;
            }
        }
        

		return false;
	}

	/**
	 * Returns true if the file is public
	 *
	 * @param string $f file to access
	 * @return bool
	 * @author Franck Royer
	 */
    public static function is_public($f){
        $judge	=	new JudgeUploader($f);
        return($judge->public);
    }



	public function infodirtoHTML($dir){
		$w 	= File::a2r($dir);
		$ret = "";

		/// Folder name
		if(is_dir($dir)){
		$ret .=	"<form class='niceform pure-form' action='?a=Upl' method='post'  onsubmit='return executeOnSubmit(`create`);'>
						<input type='hidden' name='path' value=\"".htmlentities($w, ENT_QUOTES ,'UTF-8')."\">

						<div class='pure-g'>
							<div class=' pure-u-1-2'>
								<input id='foldername' name='newdir' style='max-width:100%;' type='text' value='".Settings::_("adminpanel","new")."'>
							</div>
							<div class='pure-u-1-2'>
								<input type='submit' class='pure-button pure-button-primary' value='".Settings::_("adminpanel","create")."'>
							</div>
						</div>
					</form>";
		}

		return $ret;
	}

	/**
	 * Display the rights on website, and let
	 * the admin edit them.
	 * 
	 * @author Thibaud Rohmer
	 */
	public function toHTML(){
		
		echo "<div class='adminrights'>\n";

		echo "<h3>".Settings::_("judge","infos")."</h3>";

		echo $this->infos;

		echo "</form>\n";

        echo "</div>\n";
    }


}
?>
